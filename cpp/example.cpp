#include <X11/Xlib.h>
#include <unistd.h>
#include "xlib++/display.hpp"
using namespace xlib;
int main() {
	
  try
    {
      // Open a display.
      display d("");

      // Create the window
      Window w = XCreateWindow((Display*)d,
			       DefaultRootWindow((Display*)d),
			       0, 0, 200, 100, 0, CopyFromParent,
			       CopyFromParent, CopyFromParent, 0, 0);

      // Show the window
      XMapWindow(d, w);
      XFlush(d);

      // Sleep long enough to see the window.
      sleep(10);
    }
  catch ( open_display_exception& e )
  {
      std::cout << "Exception: " << e.what() << "\n";
  }
  //return 0;


	/*Display *d = XOpenDisplay(0);
	if (d) {
		// Create the window
		Window w = XCreateWindow(
			d, 
			DefaultRootWindow(d), 
			0,  					// left
			0,  					// top
			500,					// witch
			300,					// height
			50, 					// margin 
			CopyFromParent, 
			CopyFromParent,
			CopyFromParent, 
			0, 
			0
		);
		// Show the window
		XMapWindow(d, w);
		XFlush(d);
		// Sleep long enough to see the window.
		sleep(10);
	}*/
	return 0;
}
