import socket

server_address = ('192.168.1.108', 8686)
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setblocking(1)


server_socket.bind(server_address)
server_socket.listen(5)
print('server is running, please, press ctrl+c to stop')

while True:
	connection, address = server_socket.accept()
	print('New connection from {address}'.format(address=address))
	data = connection.recv(1024)
	print(str(data))
	connection.send(bytes('Hello from server!', encoding='UTF-8'))
	connection.close()




