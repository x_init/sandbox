import socket

address_to_server = ('192.168.1.108', 8686)

while True:
	try:
		socket_obj = socket.socket()
		socket_obj.connect(address_to_server)
		txt = input()
		socket_obj.send(bytes(txt, encoding='UTF-8'))

		data = socket_obj.recv(1024)
		print(str(data))
		#print('hello world!\n')
	except ConnectionRefusedError:
		print('Server not found!')
		break
