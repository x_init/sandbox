format ELF64
; rax - 64
; eax - 32
; ax  - 16
; ah/al - 8


public _start


; byte - 8
; word - 16
; dword - 32
; qword - 64

new_line equ 0xA



msg db "hello, world", new_line, 0
len = $-msg

_start:
	mov rax, 4 ; 4 - write
	mov rbx, 1 ; 1 - stdout
	mov rcx, msg
	mov rdx, len
	int 0x80
	call exit

exit:
	mov rax, 1 ; 1 - exit
	mov rbx, 55 ; 0 - return
	int 0x80
