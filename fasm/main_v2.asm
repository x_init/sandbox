format ELF64
; rax - 64
; eax - 32
; ax  - 16
; ah/al - 8


public _start


; byte - 8
; word - 16
; dword - 32
; qword - 64

new_line equ 0xA



msg db "hello, world", new_line, 0
len = $-msg

_start:
	mov rax, msg
	call print_string
	call exit

; | input
; rax = string
print_string:
	ret ; значит что вернуться к call print_string


; | input 
; rax = string
; | output
; rax = length
length_string:
	xor rdx, rdx
	.next_iter:
		cmp [rax+rdx], byte 0 
		jpm .next_iter
	
	ret

exit:
	mov rax, 1 ; 1 - exit
	mov rbx, 55 ; 0 - return
	int 0x80
