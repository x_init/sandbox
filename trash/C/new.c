#include <stdio.h>
#include <malloc.h>

struct car1 {
	int R;
	volatile char name[10];
	char color[10];
};

typedef struct car1 car1;
void main() {
	printf("Hello world!\n");
	struct car1 mers;
	mers.R = 5;
	printf("mers.R = %d\n", mers.R);
	car1 subaru;
	
	scanf("%s", subaru.name);
	printf("%s\n", subaru.name);
	int* arr = (int*)malloc(sizeof(int) * 8);
	for (int i=0;i<10;i++) {
		arr[i] = 1;
	}
	for (int i=0;i<10;i++) {
		printf("%#x\n", &arr[i]);
	}
	free(arr);
}
