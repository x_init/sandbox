#include <stdio.h>
#include <stdlib.h>

#define ERROR_FILE_OPEN -3

void main() {
	FILE* output = NULL;
	int number;

	output = fopen("output.bin", "wb");
	if (output == NULL) {
		printf("Error opening file\n");
		exit(ERROR_FILE_OPEN);
	}
	scanf("%d", &number);
	fwrite(&number, sizeof(int), 1, output);
	fclose(output);
}

